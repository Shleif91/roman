<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170908151414 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE cars (id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', model VARCHAR(255) DEFAULT NULL, number_car VARCHAR(255) NOT NULL, number_semitrailer VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE applications (id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', executor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', application_number INT DEFAULT NULL, application_date DATE DEFAULT NULL, `from` VARCHAR(255) NOT NULL, `to` VARCHAR(255) NOT NULL, date_load DATE DEFAULT NULL, address_load TEXT DEFAULT NULL, custom_clearance_docs_load TEXT DEFAULT NULL, date_unload DATE DEFAULT NULL, address_unload TEXT DEFAULT NULL, custom_clearance_docs_unload TEXT DEFAULT NULL, cost NUMERIC(8, 2) DEFAULT NULL, payment_date DATE DEFAULT NULL, due_date DATE DEFAULT NULL, note LONGTEXT DEFAULT NULL, INDEX IDX_F7C966F08ABD09BB (executor_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE applications_cars (application_id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', car_id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', INDEX IDX_5B184F383E030ACD (application_id), INDEX IDX_5B184F38C3C6F69F (car_id), PRIMARY KEY(application_id, car_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE applications_drivers (application_id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', driver_id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', INDEX IDX_4BA68CDE3E030ACD (application_id), INDEX IDX_4BA68CDEC3423909 (driver_id), PRIMARY KEY(application_id, driver_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE driver_phones (id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', drive_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', phone VARCHAR(255) NOT NULL, INDEX IDX_A677F04D86E5E0C4 (drive_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE executors (id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE semitrailer_types (id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', type VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE drivers (id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', full_name VARCHAR(255) NOT NULL, passport VARCHAR(255) NOT NULL, address VARCHAR(255) NOT NULL, date DATE NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE executor_phones (id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', executor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', phone VARCHAR(50) NOT NULL, INDEX IDX_A943978A8ABD09BB (executor_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE applications ADD CONSTRAINT FK_F7C966F08ABD09BB FOREIGN KEY (executor_id) REFERENCES executors (id)');
        $this->addSql('ALTER TABLE applications_cars ADD CONSTRAINT FK_5B184F383E030ACD FOREIGN KEY (application_id) REFERENCES applications (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE applications_cars ADD CONSTRAINT FK_5B184F38C3C6F69F FOREIGN KEY (car_id) REFERENCES cars (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE applications_drivers ADD CONSTRAINT FK_4BA68CDE3E030ACD FOREIGN KEY (application_id) REFERENCES applications (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE applications_drivers ADD CONSTRAINT FK_4BA68CDEC3423909 FOREIGN KEY (driver_id) REFERENCES drivers (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE driver_phones ADD CONSTRAINT FK_A677F04D86E5E0C4 FOREIGN KEY (drive_id) REFERENCES drivers (id)');
        $this->addSql('ALTER TABLE executor_phones ADD CONSTRAINT FK_A943978A8ABD09BB FOREIGN KEY (executor_id) REFERENCES executors (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE applications_cars DROP FOREIGN KEY FK_5B184F38C3C6F69F');
        $this->addSql('ALTER TABLE applications_cars DROP FOREIGN KEY FK_5B184F383E030ACD');
        $this->addSql('ALTER TABLE applications_drivers DROP FOREIGN KEY FK_4BA68CDE3E030ACD');
        $this->addSql('ALTER TABLE applications DROP FOREIGN KEY FK_F7C966F08ABD09BB');
        $this->addSql('ALTER TABLE executor_phones DROP FOREIGN KEY FK_A943978A8ABD09BB');
        $this->addSql('ALTER TABLE applications_drivers DROP FOREIGN KEY FK_4BA68CDEC3423909');
        $this->addSql('ALTER TABLE driver_phones DROP FOREIGN KEY FK_A677F04D86E5E0C4');
        $this->addSql('DROP TABLE cars');
        $this->addSql('DROP TABLE applications');
        $this->addSql('DROP TABLE applications_cars');
        $this->addSql('DROP TABLE applications_drivers');
        $this->addSql('DROP TABLE driver_phones');
        $this->addSql('DROP TABLE executors');
        $this->addSql('DROP TABLE semitrailer_types');
        $this->addSql('DROP TABLE drivers');
        $this->addSql('DROP TABLE executor_phones');
    }
}
