<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170911193327 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE applications ADD from_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', DROP `from`');
        $this->addSql('ALTER TABLE applications ADD CONSTRAINT FK_F7C966F078CED90B FOREIGN KEY (from_id) REFERENCES towns (id)');
        $this->addSql('CREATE INDEX IDX_F7C966F078CED90B ON applications (from_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE applications DROP FOREIGN KEY FK_F7C966F078CED90B');
        $this->addSql('DROP INDEX IDX_F7C966F078CED90B ON applications');
        $this->addSql('ALTER TABLE applications ADD `from` VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, DROP from_id');
    }
}
