<?php

namespace AppBundle\Api\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SmsController extends Controller
{
    /**
     * @Route("/api/send-sms")
     * @Method({"POST", "GET"})
     *
     * @param Request $request
     * @return Response
     */
    public function sendSmsAction(Request $request)
    {
        $numberAppl = $request->request->get('numberAdv');
        $phone = $request->request->get('phone');
        if ($numberAppl && $phone) {
            $application = $this->get('application.service')->getApplicationByNumber($numberAppl);
            $phone = preg_replace("/\D/", '', $phone);
            $text = $this->get('sms.service')->sendSms($application, $phone);

            return new JsonResponse($text, Response::HTTP_OK);
        }

        return new JsonResponse('Ошибка при попытке отправки сообщения', Response::HTTP_BAD_REQUEST);
    }
}