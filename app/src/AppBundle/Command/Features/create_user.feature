Feature: Manage car creation
  In application to change car
  Managers should be able to
  Create new car

  Scenario: Car can Creation with good credentials
    When I send a "POST" request to "/users/new" with body:
      """
      {
        "username": "peter",
        "password": "testpass"
      }
      """
    Then the response code should be 200
    And the response should contain "car"