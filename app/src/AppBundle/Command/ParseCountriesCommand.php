<?php

namespace AppBundle\Command;

use AppBundle\Entity\Country;
use Goutte\Client;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DomCrawler\Crawler;

class ParseCountriesCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('parse:countries')
            ->setDescription('Command for parsing countries')
            ->setHelp('This command parsing countries from http://issa.ru and saves them with codes and chars');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $client = new Client();
        $crawler = $client->request('GET', 'http://issa.ru/info/country/');
        $countryService = $this->getContainer()->get('country.service');

        $crawler->filter('.TMasterTable tr')->each(function (Crawler $node) use ($output, $countryService) {
            $country = new Country();
            $node->filter('td')->each(function (Crawler $node, $i)  use ($country) {
                if ($i == 0) {
                    $country->setCode($node->text());
                } else {
                    $country->setName($node->text());
                }
            });
        $node->filter('th')->each(function (Crawler $node)  use ($country) {
            $country->setChars($node->text());
        });

        $countryService->saveCountry($country);
        $output->writeln($country->getId() . ' ' . $country->getName());
    });
}
}