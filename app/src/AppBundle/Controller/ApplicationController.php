<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Application;
use AppBundle\Form\ApplicationType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ApplicationController
 * @package AppBundle\Controller
 *
 * @Route("/applications")
 */
class ApplicationController extends Controller
{
    /**
     * @Route("", name="applications_index")
     *
     * @param Request $request
     * @return Response
     */
    public function indexAction(Request $request)
    {
        $query = $this->get('application.service')->getAllApplicationsQuery();

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            10/*limit per page*/
        );

        return $this->render('AppBundle:Application:index.html.twig', [
            'pagination' => $pagination
        ]);
    }

    /**
     * @Route("/new", name="application_new")
     * @Method({"GET", "POST"})
     *
     * @param Request $request
     * @return Response
     */
    public function newAction(Request $request)
    {
        $application = new Application();
        $form = $this->createForm(ApplicationType::class, $application, [
            'action' => $this->generateUrl('application_new', ['id' => $application->getId()])
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $application = $form->getData();

            $this->get('application.service')->saveApplication($application, $this->getUser());

            return $this->redirectToRoute('applications_index');
        }

        return $this->render('AppBundle:Application:new.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/{id}/edit", name="application_edit")
     *
     * @param Application $application
     * @param Request $request
     * @return Response
     */
    public function editAction(Request $request, Application $application)
    {
        $form = $this->createForm(ApplicationType::class, $application, [
            'action' => $this->generateUrl('application_edit', ['id' => $application->getId()])
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $application = $form->getData();

            $this->get('application.service')->saveApplication($application, $this->getUser());

            return $this->redirectToRoute('applications_index');
        }

        return $this->render('AppBundle:Application:edit.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/{id}/view", name="application_view")
     *
     * @param Application $application
     * @param Request $request
     * @return Response
     */
    public function viewAction(Request $request, Application $application)
    {
        return $this->render('AppBundle:Application:view.html.twig', [
            'application' => $application
        ]);
    }
}