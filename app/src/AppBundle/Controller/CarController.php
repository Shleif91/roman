<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Car;
use AppBundle\Form\CarType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\Date;

/**
 * Class CarsController
 * @package AppBundle\Controller
 *
 * @Route("/cars")
 */
class CarController extends Controller
{
    /**
     * @Route("", name="cars_index")
     *
     * @param Request $request
     * @return Response
     */
    public function indexAction(Request $request)
    {
        $query = $this->get('car.service')->getAllCarsQuery();

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            10/*limit per page*/
        );

        return $this->render('AppBundle:Car:index.html.twig', [
            'pagination' => $pagination
        ]);
    }

    /**
     * @Route("/new", name="car_new")
     * @Method({"GET", "POST"})
     *
     * @param Request $request
     * @return Response
     */
    public function newAction(Request $request)
    {
        $car = new Car();
        $form = $this->createForm(CarType::class, $car, [
            'action' => $this->generateUrl('car_new', ['id' => $car->getId()])
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $car = $form->getData();

            $this->get('car.service')->saveCar($car);

            return $this->redirectToRoute('cars_index');
        }

        return $this->render('AppBundle:Car:new.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/{id}/edit", name="car_edit")
     *
     * @param Car $car
     * @param Request $request
     * @return Response
     */
    public function editAction(Request $request, Car $car)
    {
        $form = $this->createForm(CarType::class, $car, [
            'action' => $this->generateUrl('car_edit', ['id' => $car->getId()])
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $car = $form->getData();

            $this->get('car.service')->saveCar($car);

            return $this->redirectToRoute('cars_index');
        }

        return $this->render('AppBundle:Car:edit.html.twig', [
            'form' => $form->createView()
        ]);
    }
}