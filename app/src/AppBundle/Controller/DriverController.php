<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Driver;
use AppBundle\Form\DriverType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class DriversController
 * @package AppBundle\Controller
 *
 * @Route("/drivers")
 */
class DriverController extends Controller
{
    /**
     * @Route("", name="drivers_index")
     *
     * @param Request $request
     * @return Response
     */
    public function indexAction(Request $request)
    {
        $query = $this->get('driver.service')->getAllDriversQuery();

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            10/*limit per page*/
        );

        return $this->render('AppBundle:Driver:index.html.twig', [
            'pagination' => $pagination
        ]);
    }

    /**
     * @Route("/new", name="driver_new")
     * @Method({"GET", "POST"})
     *
     * @param Request $request
     * @return Response
     */
    public function newAction(Request $request)
    {
        $driver = new Driver();
        $form = $this->createForm(DriverType::class, $driver, [
            'action' => $this->generateUrl('driver_new', ['id' => $driver->getId()])
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $driver = $form->getData();

            $this->get('driver.service')->saveDriver($driver);

            return $this->redirectToRoute('drivers_index');
        }

        return $this->render('AppBundle:Driver:new.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/{id}/edit", name="driver_edit")
     *
     * @param Driver $driver
     * @param Request $request
     * @return Response
     */
    public function editAction(Request $request, Driver $driver)
    {
        $form = $this->createForm(DriverType::class, $driver, [
            'action' => $this->generateUrl('driver_edit', ['id' => $driver->getId()])
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $driver = $form->getData();

            $this->get('driver.service')->saveDriver($driver);

            return $this->redirectToRoute('drivers_index');
        }

        return $this->render('AppBundle:Driver:edit.html.twig', [
            'form' => $form->createView()
        ]);
    }
}