<?php

namespace AppBundle\Controller;

use AppBundle\Entity\DriverPhone;
use AppBundle\Form\DriverPhoneType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class DriverPhonesController
 * @package AppBundle\Controller
 *
 * @Route("/driver_phones")
 */
class DriverPhoneController extends Controller
{
    /**
     * @Route("", name="driver_phones_index")
     *
     * @param Request $request
     * @return Response
     */
    public function indexAction(Request $request)
    {
        $query = $this->get('driver_phone.service')->getAllDriverPhonesQuery();

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            10/*limit per page*/
        );

        return $this->render('AppBundle:DriverPhone:index.html.twig', [
            'pagination' => $pagination
        ]);
    }

    /**
     * @Route("/new", name="driver_phone_new")
     * @Method({"GET", "POST"})
     *
     * @param Request $request
     * @return Response
     */
    public function newAction(Request $request)
    {
        $driverPhone = new DriverPhone();
        $form = $this->createForm(DriverPhoneType::class, $driverPhone, [
            'action' => $this->generateUrl('driver_phone_new', ['id' => $driverPhone->getId()])
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $driverPhone = $form->getData();

            $this->get('driver_phone.service')->saveDriverPhone($driverPhone);

            return $this->redirectToRoute('driver_phones_index');
        }

        return $this->render('AppBundle:DriverPhone:new.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/{id}/edit", name="driver_phone_edit")
     *
     * @param DriverPhone $driverPhone
     * @param Request $request
     * @return Response
     */
    public function editAction(Request $request, DriverPhone $driverPhone)
    {
        $form = $this->createForm(DriverPhoneType::class, $driverPhone, [
            'action' => $this->generateUrl('driver_phone_edit', ['id' => $driverPhone->getId()])
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $driverPhone = $form->getData();

            $this->get('driver_phone.service')->saveDriverPhone($driverPhone);

            return $this->redirectToRoute('driver_phones_index');
        }

        return $this->render('AppBundle:DriverPhone:edit.html.twig', [
            'form' => $form->createView()
        ]);
    }
}