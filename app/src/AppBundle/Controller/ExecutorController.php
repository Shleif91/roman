<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Executor;
use AppBundle\Form\ExecutorType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ExecutorsController
 * @package AppBundle\Controller
 *
 * @Route("/executors")
 */
class ExecutorController extends Controller
{
    /**
     * @Route("", name="executors_index")
     *
     * @param Request $request
     * @return Response
     */
    public function indexAction(Request $request)
    {
        $query = $this->get('executor.service')->getAllExecutorsQuery();

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            10/*limit per page*/
        );

        return $this->render('AppBundle:Executor:index.html.twig', [
            'pagination' => $pagination
        ]);
    }

    /**
     * @Route("/new", name="executor_new")
     * @Method({"GET", "POST"})
     *
     * @param Request $request
     * @return Response
     */
    public function newAction(Request $request)
    {
        $executor = new Executor();
        $form = $this->createForm(ExecutorType::class, $executor, [
            'action' => $this->generateUrl('executor_new', ['id' => $executor->getId()])
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $executor = $form->getData();

            $this->get('executor.service')->saveExecutor($executor);

            return $this->redirectToRoute('executors_index');
        }

        return $this->render('AppBundle:Executor:new.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/{id}/edit", name="executor_edit")
     *
     * @param Executor $executor
     * @param Request $request
     * @return Response
     */
    public function editAction(Request $request, Executor $executor)
    {
        $form = $this->createForm(ExecutorType::class, $executor, [
            'action' => $this->generateUrl('executor_edit', ['id' => $executor->getId()])
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $executor = $form->getData();

            $this->get('executor.service')->saveExecutor($executor);

            return $this->redirectToRoute('executors_index');
        }

        return $this->render('AppBundle:Executor:edit.html.twig', [
            'form' => $form->createView()
        ]);
    }
}