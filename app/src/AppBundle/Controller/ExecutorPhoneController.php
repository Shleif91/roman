<?php

namespace AppBundle\Controller;

use AppBundle\Entity\ExecutorPhone;
use AppBundle\Form\ExecutorPhoneType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ExecutorPhonesController
 * @package AppBundle\Controller
 *
 * @Route("/executor_phones")
 */
class ExecutorPhoneController extends Controller
{
    /**
     * @Route("", name="executor_phones_index")
     *
     * @param Request $request
     * @return Response
     */
    public function indexAction(Request $request)
    {
        $query = $this->get('executor_phone.service')->getAllExecutorPhonesQuery();

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            10/*limit per page*/
        );

        return $this->render('AppBundle:ExecutorPhone:index.html.twig', [
            'pagination' => $pagination
        ]);
    }

    /**
     * @Route("/new", name="executor_phone_new")
     * @Method({"GET", "POST"})
     *
     * @param Request $request
     * @return Response
     */
    public function newAction(Request $request)
    {
        $executorPhone = new ExecutorPhone();
        $form = $this->createForm(ExecutorPhoneType::class, $executorPhone, [
            'action' => $this->generateUrl('executor_phone_new', ['id' => $executorPhone->getId()])
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $executorPhone = $form->getData();

            $this->get('executor_phone.service')->saveExecutorPhone($executorPhone);

            return $this->redirectToRoute('executor_phones_index');
        }

        return $this->render('AppBundle:ExecutorPhone:new.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/{id}/edit", name="executor_phone_edit")
     *
     * @param ExecutorPhone $executorPhone
     * @param Request $request
     * @return Response
     */
    public function editAction(Request $request, ExecutorPhone $executorPhone)
    {
        $form = $this->createForm(ExecutorPhoneType::class, $executorPhone, [
            'action' => $this->generateUrl('executor_phone_edit', ['id' => $executorPhone->getId()])
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $executorPhone = $form->getData();

            $this->get('executor_phone.service')->saveExecutorPhone($executorPhone);

            return $this->redirectToRoute('executor_phones_index');
        }

        return $this->render('AppBundle:ExecutorPhone:edit.html.twig', [
            'form' => $form->createView()
        ]);
    }
}