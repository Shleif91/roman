<?php

namespace AppBundle\Controller;

use AppBundle\Entity\SemitrailerType;
use AppBundle\Form\SemitrailerTypeType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class SemitrailerTypesController
 * @package AppBundle\Controller
 *
 * @Route("/semitrailer_types")
 */
class SemitrailerTypeController extends Controller
{
    /**
     * @Route("", name="semitrailer_types_index")
     *
     * @param Request $request
     * @return Response
     */
    public function indexAction(Request $request)
    {
        $query = $this->get('semitrailer_type.service')->getAllSemitrailerTypesQuery();

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            10/*limit per page*/
        );

        return $this->render('AppBundle:SemitrailerType:index.html.twig', [
            'pagination' => $pagination
        ]);
    }

    /**
     * @Route("/new", name="semitrailer_type_new")
     * @Method({"GET", "POST"})
     *
     * @param Request $request
     * @return Response
     */
    public function newAction(Request $request)
    {
        $semitrailerType = new SemitrailerType();
        $form = $this->createForm(SemitrailerTypeType::class, $semitrailerType, [
            'action' => $this->generateUrl('semitrailer_type_new', ['id' => $semitrailerType->getId()])
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $semitrailerType = $form->getData();

            $this->get('semitrailer_type.service')->saveSemitrailerType($semitrailerType);

            return $this->redirectToRoute('semitrailer_types_index');
        }

        return $this->render('AppBundle:SemitrailerType:new.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/{id}/edit", name="semitrailer_type_edit")
     *
     * @param SemitrailerType $semitrailerType
     * @param Request $request
     * @return Response
     */
    public function editAction(Request $request, SemitrailerType $semitrailerType)
    {
        $form = $this->createForm(SemitrailerTypeType::class, $semitrailerType, [
            'action' => $this->generateUrl('semitrailer_type_edit', ['id' => $semitrailerType->getId()])
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $semitrailerType = $form->getData();

            $this->get('semitrailer_type.service')->saveSemitrailerType($semitrailerType);

            return $this->redirectToRoute('semitrailer_types_index');
        }

        return $this->render('AppBundle:SemitrailerType:edit.html.twig', [
            'form' => $form->createView()
        ]);
    }
}