<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Town;
use AppBundle\Form\TownType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class TownsController
 * @package AppBundle\Controller
 *
 * @Route("/towns")
 */
class TownsController extends Controller
{
    /**
     * @Route("", name="towns_index")
     *
     * @param Request $request
     * @return Response
     */
    public function indexAction(Request $request)
    {
        $query = $this->get('town.service')->getAllTownsQuery();

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            10/*limit per page*/
        );

        return $this->render('AppBundle:Town:index.html.twig', [
            'pagination' => $pagination
        ]);
    }

    /**
     * @Route("/new", name="town_new")
     * @Method({"GET", "POST"})
     *
     * @param Request $request
     * @return Response
     */
    public function newAction(Request $request)
    {
        $town = new Town();
        $form = $this->createForm(TownType::class, $town, [
            'action' => $this->generateUrl('town_new', ['id' => $town->getId()])
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $town = $form->getData();

            $this->get('town.service')->saveTown($town);

            return $this->redirectToRoute('towns_index');
        }

        return $this->render('AppBundle:Town:new.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/{id}/edit", name="town_edit")
     *
     * @param Town $town
     * @param Request $request
     * @return Response
     */
    public function editAction(Request $request, Town $town)
    {
        $form = $this->createForm(TownType::class, $town, [
            'action' => $this->generateUrl('town_edit', ['id' => $town->getId()])
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $town = $form->getData();

            $this->get('town.service')->saveTown($town);

            return $this->redirectToRoute('towns_index');
        }

        return $this->render('AppBundle:Town:edit.html.twig', [
            'form' => $form->createView()
        ]);
    }
}