<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Executor;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadExecutorData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $executor = new Executor();
        $executor->setName('test Executor');

        $manager->persist($executor);

        $manager->flush();
    }

    public function getOrder()
    {
        return 1;
    }
}