<?php

namespace AppBundle\DataManager;

use AppBundle\Entity\Application;

class ApplicationDataManager extends EntityDataManager
{
    const ENTITY_NAME = 'AppBundle:Application';

    public function getAllApplications()
    {
        $applications = $this->repository->findAll();

        return $applications;
    }

    /**
     * @return \Doctrine\ORM\Query
     */
    public function getAllApplicationsQuery()
    {
        $query = $this->repository->getAllApplicationsQuery();

        return $query;
    }

    /**
     * @param string $number
     * @return Application|null
     */
    public function getApplicationByNumber($number)
    {
        $application = $this->repository->findOneBy(['applicationNumber' => $number]);

        return $application;
    }
}