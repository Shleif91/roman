<?php

namespace AppBundle\DataManager;

use AppBundle\Entity\Car;

class CarDataManager extends EntityDataManager
{
    const ENTITY_NAME = 'AppBundle:Car';

    /**
     * @return array|Car[]
     */
    public function getAllCars()
    {
        $users = $this->repository->findAll();

        return $users;
    }

    /**
     * @return \Doctrine\ORM\Query
     */
    public function getAllCarsQuery()
    {
        $query = $this->repository->getAllCarsQuery();

        return $query;
    }
}