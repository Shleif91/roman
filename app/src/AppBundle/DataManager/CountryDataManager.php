<?php

namespace AppBundle\DataManager;

use AppBundle\Entity\Country;
use Doctrine\ORM\EntityManagerInterface;

class CountryDataManager
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var \AppBundle\Repository\CountryRepository
     */
    protected $repository;

    /**
     * CountryDataManager constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->repository = $this->em->getRepository(Country::class);
    }

    /**
     * @param Country $country
     */
    public function save(Country $country) {
        $this->em->persist($country);
        $this->em->flush();
    }

    /**
     * @param Country $country
     */
    public function remove(Country $country) {
        $this->em->remove($country);
        $this->em->flush();
    }

    /**
     * @return \Doctrine\ORM\Query
     */
    public function getAllQuery()
    {
        $query = $this->repository->getAllQuery();

        return $query;
    }
}