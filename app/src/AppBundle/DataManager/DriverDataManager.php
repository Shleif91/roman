<?php

namespace AppBundle\DataManager;

use AppBundle\Entity\Driver;

class DriverDataManager extends EntityDataManager
{
    const ENTITY_NAME = 'AppBundle:Driver';

    /**
     * @return array|Driver[]
     */
    public function getAllDrivers()
    {
        $users = $this->repository->findAll();

        return $users;
    }

    /**
     * @return \Doctrine\ORM\Query
     */
    public function getAllDriversQuery()
    {
        $query = $this->repository->getAllDriversQuery();

        return $query;
    }
}