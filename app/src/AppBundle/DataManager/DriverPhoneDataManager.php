<?php

namespace AppBundle\DataManager;

use AppBundle\Entity\DriverPhone;

class DriverPhoneDataManager extends EntityDataManager
{
    const ENTITY_NAME = 'AppBundle:DriverPhone';

    /**
     * @return array|DriverPhone[]
     */
    public function getAllDriverPhones()
    {
        $users = $this->repository->findAll();

        return $users;
    }

    /**
     * @return \Doctrine\ORM\Query
     */
    public function getAllDriverPhonesQuery()
    {
        $query = $this->repository->getAllDriverPhonesQuery();

        return $query;
    }
}