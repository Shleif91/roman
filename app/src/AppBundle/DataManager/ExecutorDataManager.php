<?php

namespace AppBundle\DataManager;

use AppBundle\Entity\Executor;

class ExecutorDataManager extends EntityDataManager
{
    const ENTITY_NAME = 'AppBundle:Executor';

    /**
     * @return array|Executor[]
     */
    public function getAllExecutors()
    {
        $users = $this->repository->findAll();

        return $users;
    }

    /**
     * @return \Doctrine\ORM\Query
     */
    public function getAllExecutorsQuery()
    {
        $query = $this->repository->getAllExecutorsQuery();

        return $query;
    }
}