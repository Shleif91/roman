<?php

namespace AppBundle\DataManager;

use AppBundle\Entity\ExecutorPhone;

class ExecutorPhoneDataManager extends EntityDataManager
{
    const ENTITY_NAME = 'AppBundle:ExecutorPhone';

    /**
     * @return array|ExecutorPhone[]
     */
    public function getAllExecutorPhones()
    {
        $users = $this->repository->findAll();

        return $users;
    }

    /**
     * @return \Doctrine\ORM\Query
     */
    public function getAllExecutorPhonesQuery()
    {
        $query = $this->repository->getAllExecutorPhonesQuery();

        return $query;
    }
}