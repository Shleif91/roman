<?php

namespace AppBundle\DataManager;

use AppBundle\Entity\Order;

class OrderDataManager extends EntityDataManager
{
    const ENTITY_NAME = 'AppBundle:Order';

    /**
     * @return array|Order[]
     */
    public function getAllOrders()
    {
        $users = $this->repository->findAll();

        return $users;
    }

    /**
     * @return \Doctrine\ORM\Query
     */
    public function getAllOrdersQuery()
    {
        $query = $this->repository->getAllOrdersQuery();

        return $query;
    }
}