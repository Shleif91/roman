<?php

namespace AppBundle\DataManager;

use AppBundle\Entity\SemitrailerType;

class SemitrailerTypeDataManager extends EntityDataManager
{
    const ENTITY_NAME = 'AppBundle:SemitrailerType';

    /**
     * @return array|SemitrailerType[]
     */
    public function getAllSemitrailerTypes()
    {
        $users = $this->repository->findAll();

        return $users;
    }

    /**
     * @return \Doctrine\ORM\Query
     */
    public function getAllSemitrailerTypesQuery()
    {
        $query = $this->repository->getAllSemitrailerTypesQuery();

        return $query;
    }
}