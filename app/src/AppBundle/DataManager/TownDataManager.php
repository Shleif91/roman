<?php

namespace AppBundle\DataManager;

use AppBundle\Entity\Town;

class TownDataManager extends EntityDataManager
{
    const ENTITY_NAME = 'AppBundle:Town';

    /**
     * @return array|Town[]
     */
    public function getAllTowns()
    {
        $users = $this->repository->findAll();

        return $users;
    }

    /**
     * @return \Doctrine\ORM\Query
     */
    public function getAllTownsQuery()
    {
        $query = $this->repository->getAllTownsQuery();

        return $query;
    }
}