<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Application
 * @package AppBundle\Entity
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ApplicationRepository")
 * @ORM\Table(name="applications")
 */
class Application
{
    /**
     * @ORM\Id
     * @ORM\Column(type="guid")
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $applicationNumber;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $applicationDate;

    /**
     * Many Applications have One
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Executor", inversedBy="application")
     * @ORM\JoinColumn(name="executor_id", referencedColumnName="id")
     */
    private $executor;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Town")
     * @ORM\JoinColumn(name="from_id", referencedColumnName="id")
     */
    private $from;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Town")
     * @ORM\JoinColumn(name="to_id", referencedColumnName="id")
     */
    private $to;

    /**
     * Many Applications have Many Cars
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Car", inversedBy="applications")
     * @ORM\JoinTable(name="applications_cars")
     */
    private $cars;

    /**
     * Many Applications have Many Drivers
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Driver", inversedBy="applications")
     * @ORM\JoinTable(name="applications_drivers")
     */
    private $drivers;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $dateLoad;

    /**
     * @ORM\Column(type="text", length=1000, nullable=true)
     */
    private $addressLoad;

    /**
     * @ORM\Column(type="text", length=1000, nullable=true)
     */
    private $customClearanceDocsLoad;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $dateUnload;

    /**
     * @ORM\Column(type="text", length=1000, nullable=true)
     */
    private $addressUnload;

    /**
     * @ORM\Column(type="text", length=1000, nullable=true)
     */
    private $customClearanceDocsUnload;

    /**
     * @ORM\Column(type="decimal", precision=8, scale=2, nullable=true)
     */
    private $cost;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $paymentDate;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $dueDate;

    /**
     * @ORM\Column(type="text", nullable=true, nullable=true)
     */
    private $note;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * Application constructor.
     */
    public function __construct()
    {
        $this->cars = new ArrayCollection();
        $this->drivers = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getApplicationNumber()
    {
        return $this->applicationNumber;
    }

    /**
     * @param mixed $applicationNumber
     * @return Application
     */
    public function setApplicationNumber($applicationNumber)
    {
        $this->applicationNumber = $applicationNumber;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getApplicationDate()
    {
        return $this->applicationDate;
    }

    /**
     * @param mixed $applicationDate
     * @return Application
     */
    public function setApplicationDate($applicationDate)
    {
        $this->applicationDate = $applicationDate;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getExecutor()
    {
        return $this->executor;
    }

    /**
     * @param mixed $executor
     * @return Application
     */
    public function setExecutor($executor)
    {
        $this->executor = $executor;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * @param mixed $from
     * @return Application
     */
    public function setFrom($from)
    {
        $this->from = $from;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTo()
    {
        return $this->to;
    }

    /**
     * @param mixed $to
     * @return Application
     */
    public function setTo($to)
    {
        $this->to = $to;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCars()
    {
        return $this->cars;
    }

    /**
     * @param Car $car
     * @return Application
     */
    public function addCar(Car $car)
    {
        $this->cars->add($car);
        return $this;
    }

    /**
     * @param Car $car
     * @return Application
     */
    public function removeCar(Car $car)
    {
        $this->cars->removeElement($car);
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDrivers()
    {
        return $this->drivers;
    }

    /**
     * @param Driver $driver
     * @return Application
     */
    public function addDrivers(Driver $driver)
    {
        $this->drivers->add($driver);
        return $this;
    }

    /**
     * @param Driver $driver
     * @return Application
     */
    public function removeDrivers(Driver $driver)
    {
        $this->drivers->remove($driver);
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDateLoad()
    {
        return $this->dateLoad;
    }

    /**
     * @param mixed $dateLoad
     * @return Application
     */
    public function setDateLoad($dateLoad)
    {
        $this->dateLoad = $dateLoad;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAddressLoad()
    {
        return $this->addressLoad;
    }

    /**
     * @param mixed $addressLoad
     * @return Application
     */
    public function setAddressLoad($addressLoad)
    {
        $this->addressLoad = $addressLoad;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCustomClearanceDocsLoad()
    {
        return $this->customClearanceDocsLoad;
    }

    /**
     * @param mixed $customClearanceDocsLoad
     * @return Application
     */
    public function setCustomClearanceDocsLoad($customClearanceDocsLoad)
    {
        $this->customClearanceDocsLoad = $customClearanceDocsLoad;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDateUnload()
    {
        return $this->dateUnload;
    }

    /**
     * @param mixed $dateUnload
     * @return Application
     */
    public function setDateUnload($dateUnload)
    {
        $this->dateUnload = $dateUnload;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAddressUnload()
    {
        return $this->addressUnload;
    }

    /**
     * @param mixed $addressUnload
     * @return Application
     */
    public function setAddressUnload($addressUnload)
    {
        $this->addressUnload = $addressUnload;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCustomClearanceDocsUnload()
    {
        return $this->customClearanceDocsUnload;
    }

    /**
     * @param mixed $customClearanceDocsUnload
     * @return Application
     */
    public function setCustomClearanceDocsUnload($customClearanceDocsUnload)
    {
        $this->customClearanceDocsUnload = $customClearanceDocsUnload;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCost()
    {
        return $this->cost;
    }

    /**
     * @param mixed $cost
     * @return Application
     */
    public function setCost($cost)
    {
        $this->cost = $cost;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPaymentDate()
    {
        return $this->paymentDate;
    }

    /**
     * @param mixed $paymentDate
     * @return Application
     */
    public function setPaymentDate($paymentDate)
    {
        $this->paymentDate = $paymentDate;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDueDate()
    {
        return $this->dueDate;
    }

    /**
     * @param mixed $dueDate
     * @return Application
     */
    public function setDueDate($dueDate)
    {
        $this->dueDate = $dueDate;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * @param mixed $note
     * @return Application
     */
    public function setNote($note)
    {
        $this->note = $note;
        return $this;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return Application
     */
    public function setUser(User $user)
    {
        $this->user = $user;
        return $this;
    }
}