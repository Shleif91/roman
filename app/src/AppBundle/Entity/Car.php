<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Car
 * @package AppBundle\Entity
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CarRepository")
 * @ORM\Table(name="cars")
 */
class Car
{
    /**
     * @ORM\Id
     * @ORM\Column(type="guid")
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $model;

    /**
     * @ORM\Column(type="string")
     */
    private $numberCar;

    /**
     * @ORM\Column(type="string")
     */
    private $numberSemitrailer;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\SemitrailerType")
     * @ORM\JoinColumn(name="semitrailer_id", referencedColumnName="id")
     */
    private $typeSemitrailer;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Application", mappedBy="cars")
     */
    private $applications;

    /**
     * Car constructor.
     */
    public function __construct()
    {
        $this->applications = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->getModel() . ' ' . $this->getNumberCar();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * @param mixed $model
     * @return Car
     */
    public function setModel($model)
    {
        $this->model = $model;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getNumberCar()
    {
        return $this->numberCar;
    }

    /**
     * @param mixed $numberCar
     * @return Car
     */
    public function setNumberCar($numberCar)
    {
        $this->numberCar = $numberCar;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getNumberSemitrailer()
    {
        return $this->numberSemitrailer;
    }

    /**
     * @param mixed $numberSemitrailer
     * @return Car
     */
    public function setNumberSemitrailer($numberSemitrailer)
    {
        $this->numberSemitrailer = $numberSemitrailer;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTypeSemitrailer()
    {
        return $this->typeSemitrailer;
    }

    /**
     * @param mixed $typeSemitrailer
     * @return Car
     */
    public function setTypeSemitrailer($typeSemitrailer)
    {
        $this->typeSemitrailer = $typeSemitrailer;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getApplications()
    {
        return $this->applications;
    }

    /**
     * @param Application $application
     * @return Car
     */
    public function addApplications(Application $application)
    {
        $this->applications->add($application);
        return $this;
    }

    /**
     * @param Application $application
     * @return Car
     */
    public function removeApplications(Application $application)
    {
        $this->applications->removeElement($application);
        return $this;
    }
}