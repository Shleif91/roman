<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Country
 * @package AppBundle\Entity
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CountryRepository")
 * @ORM\Table(name="countries")
 */
class Country
{
    /**
     * @ORM\Id
     * @ORM\Column(type="guid")
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @ORM\Column(type="string")
     */
    private $code;

    /**
     * @ORM\Column(type="string")
     */
    private $chars;

    /**
     * One Country has Many Towns
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Town", mappedBy="country")
     */
    private $towns;

    /**
     * Country constructor.
     */
    public function __construct()
    {
        $this->towns = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getName();
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Country
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     * @return Country
     */
    public function setCode($code)
    {
        $this->code = $code;
        return $this;
    }

    /**
     * @return string
     */
    public function getChars()
    {
        return $this->chars;
    }

    /**
     * @param string $chars
     * @return Country
     */
    public function setChars($chars)
    {
        $this->chars = $chars;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTowns()
    {
        return $this->towns;
    }

    /**
     * @param Town $town
     * @return Country
     */
    public function addTown(Town $town)
    {
        $this->towns->add($town);
        return $this;
    }

    /**
     * @param Town $town
     * @return Country
     */
    public function removeTown(Town $town)
    {
        $this->towns->removeElement($town);
        return $this;
    }
}