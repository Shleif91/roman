<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Driver
 * @package AppBundle\Entity
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\DriverRepository")
 * @ORM\Table(name="drivers")
 */
class Driver
{
    /**
     * @ORM\Id
     * @ORM\Column(type="guid")
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $fullName;

    /**
     * @ORM\Column(type="string")
     */
    private $passport;

    /**
     * @ORM\Column(type="string")
     */
    private $address;

    /**
     * @ORM\Column(type="date")
     */
    private $date;

    /**
     * One Driver has Many Phones
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\DriverPhone", mappedBy="driver")
     */
    private $phones;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Application", mappedBy="drivers")
     */
    private $applications;

    /**
     * Driver constructor.
     */
    public function __construct()
    {
        $this->phones = new ArrayCollection();
        $this->applications = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->fullName;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getFullName()
    {
        return $this->fullName;
    }

    /**
     * @param mixed $fullName
     * @return Driver
     */
    public function setFullName($fullName)
    {
        $this->fullName = $fullName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPassport()
    {
        return $this->passport;
    }

    /**
     * @param mixed $passport
     * @return Driver
     */
    public function setPassport($passport)
    {
        $this->passport = $passport;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     * @return Driver
     */
    public function setAddress($address)
    {
        $this->address = $address;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     * @return Driver
     */
    public function setDate($date)
    {
        $this->date = $date;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPhones()
    {
        return $this->phones;
    }

    /**
     * @param DriverPhone $phone
     * @return Driver
     */
    public function addPhone(DriverPhone $phone)
    {
        $this->phones->add($phone);
        return $this;
    }

    /**
     * @param DriverPhone $phone
     * @return Driver
     */
    public function removePhone(DriverPhone $phone)
    {
        $this->phones->removeElement($phone);
        return $this;
    }

    /**
     * @return mixed
     */
    public function getApplications()
    {
        return $this->applications;
    }

    /**
     * @param Application $application
     * @return Driver
     */
    public function addApplications(Application $application)
    {
        $this->applications->add($application);
        return $this;
    }

    /**
     * @param Application $application
     * @return Driver
     */
    public function removeApplications(Application $application)
    {
        $this->applications->removeElement($application);
        return $this;
    }
}