<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class DriverPhone
 * @package AppBundle\Entity
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\DriverPhoneRepository")
 * @ORM\Table(name="driver_phones")
 */
class DriverPhone
{
    /**
     * @ORM\Id
     * @ORM\Column(type="guid")
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $phone;

    /**
     * Many Phones have One Driver
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Driver", inversedBy="phones")
     * @ORM\JoinColumn(name="drive_id", referencedColumnName="id")
     */
    private $driver;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return Driver
     */
    public function getDriver()
    {
        return $this->driver;
    }

    /**
     * @param Driver $driver
     * @return DriverPhone
     */
    public function setDriver(Driver $driver)
    {
        $this->driver = $driver;
        return $this;
    }
}