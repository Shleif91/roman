<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Executor
 * @package AppBundle\Entity
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ExecutorRepository")
 * @ORM\Table(name="executors")
 */
class Executor
{
    /**
     * @ORM\Id
     * @ORM\Column(type="guid")
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * One Executor has Many Phones
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\ExecutorPhone", mappedBy="executor")
     */
    private $phones;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Application", mappedBy="executor")
     */
    private $application;

    /**
     * Executor constructor.
     */
    public function __construct()
    {
        $this->phones = new ArrayCollection();
        $this->application = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getName();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return Executor
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPhones()
    {
        return $this->phones;
    }

    /**
     * @param ExecutorPhone $phone
     * @return Executor
     */
    public function addPhone(ExecutorPhone $phone)
    {
        $this->phones->add($phone);
        return $this;
    }

    /**
     * @param ExecutorPhone $phone
     * @return Executor
     */
    public function removePhone(ExecutorPhone $phone)
    {
        $this->phones->removeElement($phone);
        return $this;
    }

    /**
     * @return mixed
     */
    public function getApplication()
    {
        return $this->application;
    }

    /**
     * @param Application $application
     * @return Executor
     */
    public function addApplication(Application $application)
    {
        $this->application->add($application);

        return $this;
    }

    /**
     * @param Application $application
     * @return Executor
     */
    public function removeApplication(Application $application)
    {
        $this->application->removeElement($application);

        return $this;
    }
}