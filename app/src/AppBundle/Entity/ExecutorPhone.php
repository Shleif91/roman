<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class ExecutorPhones
 * @package AppBundle\Entity
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ExecutorPhoneRepository")
 * @ORM\Table(name="executor_phones")
 */
class ExecutorPhone
{
    /**
     * @ORM\Id
     * @ORM\Column(type="guid")
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $phone;

    /**
     * Many Phones have One Executor
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Executor", inversedBy="phones")
     * @ORM\JoinColumn(name="executor_id", referencedColumnName="id")
     */
    private $executor;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     * @return ExecutorPhone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getExecutor()
    {
        return $this->executor;
    }

    /**
     * @param mixed $executor
     * @return ExecutorPhone
     */
    public function setExecutor($executor)
    {
        $this->executor = $executor;
        return $this;
    }
}