<?php

namespace AppBundle\Form;

use AppBundle\Entity\Application;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ApplicationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('applicationNumber', null, [
                'label' => 'phrases.applicationNumber'
            ])
            ->add('applicationDate', DateType::class, [
                'widget' => 'single_text',
                'html5' => false,
                'label' => 'phrases.fromDate',
                'attr' => ['class' => 'js-datepicker'],
            ])
            ->add('from', null, [
                'label' => 'tableHeader.From'
            ])
            ->add('to', null, [
                'label' => 'tableHeader.To'
            ])
            ->add('cost', null, [
                'label' => 'tableHeader.Cost'
            ])
            ->add('executor', null, [
                'label' => 'labels.executor'
            ])
            ->add('cars', null, [
                'label' => 'titles.cars'
            ])
            ->add('drivers', null, [
                'label' => 'titles.drivers'
            ])
            ->add('dateLoad', DateType::class, [
                'widget' => 'single_text',
                'html5' => false,
                'label' => 'labels.dateLoad',
                'attr' => ['class' => 'js-datepicker']
            ])
            ->add('addressLoad', null, [
                'label' => 'labels.addressLoad'
            ])
            ->add('customClearanceDocsLoad', null, [
                'label' => 'labels.customClearanceDocsLoad'
            ])
            ->add('dateUnload', DateType::class, [
                'widget' => 'single_text',
                'html5' => false,
                'label' => 'labels.dateUnload',
                'attr' => ['class' => 'js-datepicker']
            ])
            ->add('addressUnload', null, [
                'label' => 'labels.addressUnload'
            ])
            ->add('customClearanceDocsUnload', null, [
                'label' => 'labels.customClearanceDocsUnload'
            ])
            ->add('paymentDate', DateType::class, [
                'widget' => 'single_text',
                'html5' => false,
                'label' => 'labels.paymentDate',
                'attr' => ['class' => 'js-datepicker']
            ])
            ->add('dueDate', null, [
                'label' => 'labels.dueDate'
            ])
            ->add('note', null, [
                'label' => 'labels.note'
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Application::class,
        ]);
    }
}