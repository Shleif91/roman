<?php

namespace AppBundle\Form;

use AppBundle\Entity\Driver;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DriverType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('fullName')
            ->add('passport',  null, [
                'label' => 'Passport №'
            ])
            ->add('address', null, [
                'label' => 'Who issued passport'
            ])
            ->add('date', DateType::class, [
                'widget' => 'single_text',
                'html5' => false,
                'label' => 'Date of issue',
                'attr' => ['class' => 'js-datepicker'],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Driver::class,
        ]);
    }
}