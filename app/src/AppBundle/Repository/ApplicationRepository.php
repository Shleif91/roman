<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class ApplicationRepository extends EntityRepository
{
    /**
     * @return \Doctrine\ORM\Query
     */
    public function getAllApplicationsQuery()
    {
        $query = $this->createQueryBuilder('application')->getQuery();

        return $query;
    }
}