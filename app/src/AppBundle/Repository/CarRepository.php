<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class CarRepository extends EntityRepository
{
    /**
     * @return \Doctrine\ORM\Query
     */
    public function getAllCarsQuery()
    {
        $query = $this->createQueryBuilder('car')->getQuery();

        return $query;
    }
}