<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Country;
use Doctrine\ORM\EntityRepository;

class CountryRepository extends EntityRepository
{
    /** @return \Doctrine\ORM\Query */
    public function getAllQuery()
    {
        $query = $this->createQueryBuilder('country')->getQuery();

        return $query;
    }

    /** @return Country|null */
    public function findOne()
    {
        /** @var Country $country */
        $country = $this->findOneBy([]);

        return $country;
    }
}