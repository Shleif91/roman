<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class DriverPhoneRepository extends EntityRepository
{
    /**
     * @return \Doctrine\ORM\Query
     */
    public function getAllDriverPhonesQuery()
    {
        $query = $this->createQueryBuilder('driver_phone')->getQuery();

        return $query;
    }
}