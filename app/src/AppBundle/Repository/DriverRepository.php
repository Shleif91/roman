<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class DriverRepository extends EntityRepository
{
    /**
     * @return \Doctrine\ORM\Query
     */
    public function getAllDriversQuery()
    {
        $query = $this->createQueryBuilder('driver')->getQuery();

        return $query;
    }
}