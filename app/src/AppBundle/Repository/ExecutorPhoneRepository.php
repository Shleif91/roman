<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class ExecutorPhoneRepository extends EntityRepository
{
    /**
     * @return \Doctrine\ORM\Query
     */
    public function getAllExecutorPhonesQuery()
    {
        $query = $this->createQueryBuilder('executor_phone')
            ->getQuery();

        return $query;
    }
}