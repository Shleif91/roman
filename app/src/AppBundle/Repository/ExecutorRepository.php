<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class ExecutorRepository extends EntityRepository
{
    /**
     * @return \Doctrine\ORM\Query
     */
    public function getAllExecutorsQuery()
    {
        $query = $this->createQueryBuilder('executor')->getQuery();

        return $query;
    }
}