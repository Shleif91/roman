<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class OrderRepository extends EntityRepository
{
    /**
     * @return \Doctrine\ORM\Query
     */
    public function getAllOrdersQuery()
    {
        return $this->createQueryBuilder('o')
            ->join('o.user', 'u')
            ->getQuery();
    }
}