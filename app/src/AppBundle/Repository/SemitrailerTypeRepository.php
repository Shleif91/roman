<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class SemitrailerTypeRepository extends EntityRepository
{
    /**
     * @return \Doctrine\ORM\Query
     */
    public function getAllSemitrailerTypesQuery()
    {
        $query = $this->createQueryBuilder('semitrailer_type')->getQuery();

        return $query;
    }
}