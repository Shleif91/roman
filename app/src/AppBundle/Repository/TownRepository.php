<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class TownRepository extends EntityRepository
{
    /**
     * @return \Doctrine\ORM\Query
     */
    public function getAllTownsQuery()
    {
        $query = $this->createQueryBuilder('town')
            ->join('town.country', 'town_country')
            ->getQuery();

        return $query;
    }
}