<?php

namespace AppBundle\Service;

use AppBundle\DataManager\ApplicationDataManager;
use AppBundle\Entity\Application;
use AppBundle\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ApplicationService
{
    /**
     * @var ApplicationDataManager
     */
    protected $dataManager;

    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * ApplicationService constructor
     *
     * @param ApplicationDataManager $dataManager
     * @param ContainerInterface $container
     */
    public function __construct(ApplicationDataManager $dataManager, ContainerInterface $container)
    {
        $this->dataManager = $dataManager;
        $this->container = $container;
    }

    /**
     * @return array|Application[]
     */
    public function getApplications()
    {
        $applications = $this->dataManager->getAllApplications();

        return $applications;
    }

    /**
     * @return \Doctrine\ORM\Query
     */
    public function getAllApplicationsQuery()
    {
        $applications = $this->dataManager->getAllApplicationsQuery();

        return $applications;
    }

    /**
     * @param string $number
     * @return Application|null
     */
    public function getApplicationByNumber($number)
    {
        $application = $this->dataManager->getApplicationByNumber($number);

        return $application;
    }

    /**
     * @param Application $application
     * @param User $user
     */
    public function saveApplication(Application $application, User $user)
    {
        $application->setUser($user);
        $this->dataManager->save($application);
    }

    /**
     * @param Application $application
     */
    public function deleteApplication(Application $application)
    {
        $this->dataManager->delete($application);
    }
}