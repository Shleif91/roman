<?php

namespace AppBundle\Service;

use AppBundle\DataManager\CarDataManager;
use AppBundle\Entity\Car;
use Symfony\Component\DependencyInjection\ContainerInterface;

class CarService
{
    /**
     * @var CarDataManager
     */
    protected $dataManager;

    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * CarService constructor
     *
     * @param CarDataManager $dataManager
     * @param ContainerInterface $container
     */
    public function __construct(CarDataManager $dataManager, ContainerInterface $container)
    {
        $this->dataManager = $dataManager;
        $this->container = $container;
    }

    /**
     * @return array|Car[]
     */
    public function getCars()
    {
        $cars = $this->dataManager->getAllCars();

        return $cars;
    }

    /**
     * @return \Doctrine\ORM\Query
     */
    public function getAllCarsQuery()
    {
        $cars = $this->dataManager->getAllCarsQuery();

        return $cars;
    }

    /**
     * @param Car $car
     */
    public function saveCar(Car $car)
    {
        $this->dataManager->save($car);
    }

    /**
     * @param Car $car
     */
    public function deleteCar(Car $car)
    {
        $this->dataManager->delete($car);
    }
}