<?php

namespace AppBundle\Service;

use AppBundle\DataManager\CountryDataManager;
use AppBundle\Entity\Country;

class CountryService
{
    /**
     * @var CountryDataManager
     */
    protected $dataManager;

    /**
     * CountryService constructor.
     * @param CountryDataManager $dataManager
     */
    public function __construct(CountryDataManager $dataManager)
    {
        $this->dataManager = $dataManager;
    }

    /**
     * @param Country $country
     */
    public function saveCountry(Country $country)
    {
        $this->dataManager->save($country);
    }

    /**
     * @return \Doctrine\ORM\Query
     */
    public function getAllQuery()
    {
        $query = $this->dataManager->getAllQuery();

        return $query;
    }
}