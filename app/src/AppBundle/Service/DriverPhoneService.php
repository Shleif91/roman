<?php

namespace AppBundle\Service;

use AppBundle\DataManager\DriverPhoneDataManager;
use AppBundle\Entity\DriverPhone;
use Symfony\Component\DependencyInjection\ContainerInterface;

class DriverPhoneService
{
    /**
     * @var DriverPhoneDataManager
     */
    protected $dataManager;

    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * DriverService constructor
     *
     * @param DriverPhoneDataManager $dataManager
     * @param ContainerInterface $container
     */
    public function __construct(DriverPhoneDataManager $dataManager, ContainerInterface $container)
    {
        $this->dataManager = $dataManager;
        $this->container = $container;
    }

    /**
     * @return array|DriverPhone[]
     */
    public function getAllDriverPhones()
    {
        $drivers = $this->dataManager->getAllDriverPhones();

        return $drivers;
    }

    /**
     * @return \Doctrine\ORM\Query
     */
    public function getAllDriverPhonesQuery()
    {
        $drivers = $this->dataManager->getAllDriverPhonesQuery();

        return $drivers;
    }

    /**
     * @param DriverPhone $driverPhone
     */
    public function saveDriverPhone(DriverPhone $driverPhone)
    {
        $this->dataManager->save($driverPhone);
    }

    /**
     * @param DriverPhone $driverPhone
     */
    public function deleteDriverPhone(DriverPhone $driverPhone)
    {
        $this->dataManager->delete($driverPhone);
    }
}