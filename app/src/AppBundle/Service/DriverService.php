<?php

namespace AppBundle\Service;

use AppBundle\DataManager\DriverDataManager;
use AppBundle\Entity\Driver;
use Symfony\Component\DependencyInjection\ContainerInterface;

class DriverService
{
    /**
     * @var DriverDataManager
     */
    protected $dataManager;

    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * DriverService constructor
     *
     * @param DriverDataManager $dataManager
     * @param ContainerInterface $container
     */
    public function __construct(DriverDataManager $dataManager, ContainerInterface $container)
    {
        $this->dataManager = $dataManager;
        $this->container = $container;
    }

    /**
     * @return array|Driver[]
     */
    public function getDrivers()
    {
        $drivers = $this->dataManager->getAllDrivers();

        return $drivers;
    }

    /**
     * @return \Doctrine\ORM\Query
     */
    public function getAllDriversQuery()
    {
        $drivers = $this->dataManager->getAllDriversQuery();

        return $drivers;
    }

    /**
     * @param Driver $driver
     */
    public function saveDriver(Driver $driver)
    {
        $this->dataManager->save($driver);
    }

    /**
     * @param Driver $driver
     */
    public function deleteDriver(Driver $driver)
    {
        $this->dataManager->delete($driver);
    }
}