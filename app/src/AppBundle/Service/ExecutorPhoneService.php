<?php

namespace AppBundle\Service;

use AppBundle\DataManager\ExecutorPhoneDataManager;
use AppBundle\Entity\ExecutorPhone;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ExecutorPhoneService
{
    /**
     * @var ExecutorPhoneDataManager
     */
    protected $dataManager;

    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * ExecutorService constructor
     *
     * @param ExecutorPhoneDataManager $dataManager
     * @param ContainerInterface $container
     */
    public function __construct(ExecutorPhoneDataManager $dataManager, ContainerInterface $container)
    {
        $this->dataManager = $dataManager;
        $this->container = $container;
    }

    /**
     * @return array|ExecutorPhone[]
     */
    public function getAllExecutorPhones()
    {
        $executors = $this->dataManager->getAllExecutorPhones();

        return $executors;
    }

    /**
     * @return \Doctrine\ORM\Query
     */
    public function getAllExecutorPhonesQuery()
    {
        $executors = $this->dataManager->getAllExecutorPhonesQuery();

        return $executors;
    }

    /**
     * @param ExecutorPhone $executorPhone
     */
    public function saveExecutorPhone(ExecutorPhone $executorPhone)
    {
        $this->dataManager->save($executorPhone);
    }

    /**
     * @param ExecutorPhone $executorPhone
     */
    public function deleteExecutorPhone(ExecutorPhone $executorPhone)
    {
        $this->dataManager->delete($executorPhone);
    }
}