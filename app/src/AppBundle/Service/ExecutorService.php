<?php

namespace AppBundle\Service;

use AppBundle\DataManager\ExecutorDataManager;
use AppBundle\Entity\Executor;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ExecutorService
{
    /**
     * @var ExecutorDataManager
     */
    protected $dataManager;

    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * ExecutorService constructor
     *
     * @param ExecutorDataManager $dataManager
     * @param ContainerInterface $container
     */
    public function __construct(ExecutorDataManager $dataManager, ContainerInterface $container)
    {
        $this->dataManager = $dataManager;
        $this->container = $container;
    }

    /**
     * @return array|Executor[]
     */
    public function getExecutors()
    {
        $executors = $this->dataManager->getAllExecutors();

        return $executors;
    }

    /**
     * @return \Doctrine\ORM\Query
     */
    public function getAllExecutorsQuery()
    {
        $executors = $this->dataManager->getAllExecutorsQuery();

        return $executors;
    }

    /**
     * @param Executor $executor
     */
    public function saveExecutor(Executor $executor)
    {
        $this->dataManager->save($executor);
    }

    /**
     * @param Executor $executor
     */
    public function deleteExecutor(Executor $executor)
    {
        $this->dataManager->delete($executor);
    }
}