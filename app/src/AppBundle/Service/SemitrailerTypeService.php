<?php

namespace AppBundle\Service;

use AppBundle\DataManager\SemitrailerTypeDataManager;
use AppBundle\Entity\SemitrailerType;
use Symfony\Component\DependencyInjection\ContainerInterface;

class SemitrailerTypeService
{
    /**
     * @var SemitrailerTypeDataManager
     */
    protected $dataManager;

    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * SemitrailerTypeService constructor
     *
     * @param SemitrailerTypeDataManager $dataManager
     * @param ContainerInterface $container
     */
    public function __construct(SemitrailerTypeDataManager $dataManager, ContainerInterface $container)
    {
        $this->dataManager = $dataManager;
        $this->container = $container;
    }

    /**
     * @return array|SemitrailerType[]
     */
    public function getAllSemitrailerTypes()
    {
        $semitrailerTypes = $this->dataManager->getAllSemitrailerTypes();

        return $semitrailerTypes;
    }

    /**
     * @return \Doctrine\ORM\Query
     */
    public function getAllSemitrailerTypesQuery()
    {
        $semitrailerTypes = $this->dataManager->getAllSemitrailerTypesQuery();

        return $semitrailerTypes;
    }

    /**
     * @param SemitrailerType $semitrailerType
     */
    public function saveSemitrailerType(SemitrailerType $semitrailerType)
    {
        $this->dataManager->save($semitrailerType);
    }

    /**
     * @param SemitrailerType $semitrailerType
     */
    public function deleteSemitrailerType(SemitrailerType $semitrailerType)
    {
        $this->dataManager->delete($semitrailerType);
    }
}