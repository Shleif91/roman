<?php

namespace AppBundle\Service;

use AppBundle\Entity\Application;
use GuzzleHttp\Client;

class SmsService
{
    /** @var string $apiKey */
    private $apiKey;

    private $url = 'http://sms.unisender.by/api/v1/';

    /**
     * SmsService constructor.
     * @param $apiKey
     */
    public function __construct($apiKey)
    {
        $this->apiKey = $apiKey;
    }

    /**
     * @param string $number
     * @return string
     */
    public function sendSms(Application $application, $number)
    {
        $message = 'дата оплаты ' . $application->getPaymentDate()->format('d/m/Y');
        $params = [
            'token' => $this->apiKey,
            'message' => $message
        ];

        $url = $this->url . 'createSmsMessage?' . http_build_query($params);
        $client = new Client();
        $res = $client->request('GET', $url);
        $arrResponse = json_decode($res->getBody()->getContents(), true)[0];

        $params = [
            'token' => $this->apiKey,
            'message_id' => $arrResponse['message_id'],
            'phone' => $number
        ];
        $url = $this->url . 'sendSms?' . http_build_query($params);

        try {
            $client->request('GET', $url);
        } catch (\Exception $e) {
            return 'Возникла ошибка';
        }

        return 'Сообщение успешно отправлено';
    }
}