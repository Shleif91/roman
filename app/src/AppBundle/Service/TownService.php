<?php

namespace AppBundle\Service;

use AppBundle\DataManager\TownDataManager;
use AppBundle\Entity\Country;
use AppBundle\Entity\Town;
use Symfony\Component\DependencyInjection\ContainerInterface;

class TownService
{
    /**
     * @var TownDataManager
     */
    protected $dataManager;

    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * TownService constructor
     *
     * @param TownDataManager $dataManager
     * @param ContainerInterface $container
     */
    public function __construct(TownDataManager $dataManager, ContainerInterface $container)
    {
        $this->dataManager = $dataManager;
        $this->container = $container;
    }

    /**
     * @return array|Town[]
     */
    public function getTowns()
    {
        $towns = $this->dataManager->getAllTowns();

        return $towns;
    }

    /**
     * @return \Doctrine\ORM\Query
     */
    public function getAllTownsQuery()
    {
        $towns = $this->dataManager->getAllTownsQuery();

        return $towns;
    }

    /**
     * @param Town $town
     */
    public function saveTown(Town $town)
    {
        $this->dataManager->save($town);
    }

    /**
     * @param Town $town
     */
    public function deleteTown(Town $town)
    {
        $this->dataManager->delete($town);
    }
}